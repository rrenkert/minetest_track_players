local time_interval = 1.0
local fifo_path = "/tmp/mt_players_fifo"

function players_data()
	local ps = minetest.get_connected_players()
	local pcount = #ps
	if pcount == 0 then
		return "[]\n"
	end
	for i = 1,pcount do
		local player = ps[i]
		local data = player:getpos()
		data.name = player:get_player_name()
		ps[i] = data
	end
	return minetest.write_json(ps) .. "\n"
end

function time_interval_func()
	local fifo = io.open(fifo_path, "w")
	if fifo then
		fifo:write(players_data())
		fifo:close()
	end
	minetest.after(time_interval, time_interval_func)
end

minetest.after(time_interval, time_interval_func)
